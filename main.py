import random

from flask import Flask, request, render_template, redirect
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:root@localhost/guidesimps'
db = SQLAlchemy(app)


class Guia(db.Model):
    __tablename__ = 'thesimps'
    codigo = db.Column('codigo', db.String(50), primary_key=True)
    ep_name_ptbr = db.Column('ep_name_ptbr', db.String(100))
    ep_name_eng = db.Column('ep_name_eng', db.String(100))
    num_ep_temp = db.Column('num_ep_temp', db.Integer)
    num_ep = db.Column('num_ep', db.Integer)
    data_lancamento = db.Column('data_lancamento', db.DateTime)
    temp = db.Column('temp', db.Integer)


def linkBuilder(eplist):
    linkList = []
    for i in eplist:
        elemento = "http://www.superanimes.com/os-simpsons/episodio-"+str(i)
        linkList.append(elemento)
    return linkList

def randsName(rands):
    numbers = []
    for x in range(rands):
        numbers.append(random.randint(1, 618))
    return numbers

def findEps(numEps):
    listaFinal = []
    for x in numEps:
        episodio = Guia.query.filter_by(num_ep=x).first()
        listaFinal.append(episodio)
    return listaFinal

@app.route("/")
def index():
    return render_template("index.html")


@app.route("/episodios", methods=["POST", "GET"])
def episodios():
    list_eps = []
    if request.method == "POST":
        rands = int(request.form["qtElements"])
        print(request.form["qtElements"])
        eplist = []
        eplist = randsName(rands)
        linkList = linkBuilder(eplist)
        final = findEps(eplist)
        return render_template("episodios.html", epsList = final,links = linkList)


    else:
        pass
    return redirect("/")


if __name__ == "__main__":
    app.run(debug=True)
